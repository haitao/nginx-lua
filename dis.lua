local uri_args = ngx.req.get_uri_args()
local productId = uri_args["productId"]
local shopId = uri_args["shopId"]

local host = {"10.199.242.159:81", "10.199.242.160"}
local hash = ngx.crc32_long(productId)
hash = (hash % 2) + 1
backend = "http://"..host[hash]

local path=uri_args["path"]
local requestBody = "/"..path.."?productId="..productId.."&shopId="..shopId

local http = require("resty.http")
local httpc = http.new()

local resp, err = httpc:request_uri(backend, {
	method = "GET",
        path = requestBody
	})

if not resp then 
	ngx.say("Request err",err)
	return
end

ngx.say(resp.body)

httpc:close()

